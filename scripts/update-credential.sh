AWS_ACCOUNT=$(aws sts get-caller-identity --query 'Account' | tr -d '"')
DOCKER_REGISTRY_SERVER=https://${AWS_ACCOUNT}.dkr.ecr.{{ .Values.aws.region }}.amazonaws.com
DOCKER_USER=AWS
DOCKER_PASSWORD=`aws ecr get-login-password --region ${AWS_REGION}`
kubectl delete secret {{ .Values.secretName }} || true
kubectl create secret docker-registry {{ .Values.secretName }} \
  --docker-server="$DOCKER_REGISTRY_SERVER" \
  --docker-username="$DOCKER_USER" \
  --docker-password="$DOCKER_PASSWORD" \
  --docker-email=no@email.local