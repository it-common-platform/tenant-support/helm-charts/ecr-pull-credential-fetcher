# ECR Pull Credential Fetcher

This Helm chart defines a Job/CronJob that fetches and stores an ECR credential to ensure images can always be pulled from ECR.

## Pre-Requisites

In order to obtain an ECR credential, an IAM role needs to be defined and provided. In order to allow pods to assume a role in your account, you will need to do a little bit of configuration in your AWS account. 

You can use the following Terraform to set that up. It will create the OIDC provider, a role that can be assumed by the ServiceAccount being used by the Job/CronJob, and then give that role the ECR read-only policy.

```hcl
module "plaform-role-connector" {
  source                   = "git@code.vt.edu:it-common-platform/support/terraform/aws-platform-role-connector.git"
  role_name                = "ecr-credential-fetcher"
  role_description         = "A role for the Common Platform to fetch ECR credentials"
  k8s_namespace            = "<your-tenant-identifier>"
  k8s_service_account_name = "ecr-cred-updater"

  role_tags = {
    ResponseParty = "<your-pid>"
  }
}

# Attach the ECR Read-Only policy to the role created by the Role Connector
resource "aws_iam_role_policy_attachment" "ecr_read_only" {
  role       = module.platform-role-connector.role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
```

## Configuration

In order to deploy this Helm chart, the following values need to be defined:

```yaml
aws:
  iamRoleArn: the-role-to-assume
```

There are other options available to override and documented in the [values.yaml](./values.yaml) file.

## Deploying the Chart

If you wish to deploy using the CLI, you only need to define the repo and install the chart.

```shell
# Add the repository definition
helm repo add ecr-pull-credential-fetcher https://code.vt.edu/api/v4/projects/14546/packages/helm/stable

# Fetch the repository index
helm repo update

# Install the chart, giving it value overrides
helm install -v ./value-overrides.yaml --generate-name ecr-pull-credential-fetcher/ecr-pull-credential-fetcher
```

### Flux Installation

If you wish to install it using Flux, you can use the following:

```yaml
apiVersion: source.toolkit.fluxcd.io/v1
kind: HelmRepository
metadata:
  name: ecr-pull-credential-fetcher
spec:
  url: https://code.vt.edu/api/v4/projects/14546/packages/helm/stable
  interval: 1h
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: ecr-pull-credential-fetcher
spec:
  interval: 1h
  chart:
    spec:
      chart: ecr-pull-credential-fetcher
      version: "0.1.0"
      sourceRef:
        kind: HelmRepository
        name: ecr-pull-credential-fetcher
      interval: 1h
  values:
    aws:
      iamRoleArn: <your-iam-role-arn>
```


## Using the Credentials

Once the Helm chart is deployed, it will start a Job/CronJob. Upon successful runs, a Kubernetes Secret will be created/updated that contains the credentials. By default, that secret is named `aws-registry`. That simply needs to be plugged in the `imagePullSecrets` configuration of your Pod.

### Example

```yaml
apiVersion: v1
kind: Pod
spec:
  imagePullSecrets:
    - name: aws-registry
  containers:
    - name: app
      image: 123456789012.dkr.ecr.us-east-1.amazonaws.com/my-app:latest
```

## History

| Version | Notes |
|---------|-------|
| 0.1.0   | Initial release |
